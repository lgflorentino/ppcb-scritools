import os
from pathlib import PurePath
import argparse

from scripts import *

cli = argparse.ArgumentParser(description='')
cli.add_argument('command', type=str, nargs=1, 
        help='Script to run. Scripts are found in the \'./scripts\' directory')
args = cli.parse_args()

scr = PurePath('scripts', args.command[0])
print(scr)


