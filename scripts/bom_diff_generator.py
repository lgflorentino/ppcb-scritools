from pathlib import Path
import json
from openpyxl import Workbook

bom = Workbook()
template = open('json/bill-of-materials/templates/base-template.json', 'r')
tp = json.load(template)

print(tp['column-names'])

if __name__ == "__main__":
    import sys
    bom_diff_generator(int(sys.argv[1]))
